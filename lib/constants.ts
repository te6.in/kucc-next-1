export const API_URL = 'https://not-woowacourse-api.yopark.dev';

export const CLIENT_NAME = process.env.NEXT_PUBLIC_CLIENT_NAME;

export const DEFAULT_HEADERS = {
  'Content-Type': 'application/json',
  'client-name': CLIENT_NAME,
};
